﻿using System;

namespace RiseFood.WebApi.Models
{
    public class Category : Entity
    {
        public Category(int code, string name)
        {
            Code = code;
            Name = name;
        }

        protected Category() { }

        public int Code { get; private set; }
        public string Name { get; private set; }
        public DateTime CreatedDate { get; private set; }

        public override string ToString()
        {
            return $"{Code} - {Name}";
        }
    }
}