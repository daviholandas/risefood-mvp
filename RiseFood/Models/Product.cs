﻿using RiseFood.WebApi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Models
{
    public class Product : Entity
    {
        public Product(int code, string name, decimal price, string description, HashSet<string> ingredientsAdditionals, Sizes? size, DateTime? createdDate, Guid categoryId)
        {
            Code = code;
            Name = name;
            Price = price;
            Description = description;
            IngredientsAdditionals = ingredientsAdditionals ?? new HashSet<string>();
            Size = size ?? Sizes.Standard;
            CategoryId = categoryId;
            CreatedDate = createdDate ?? DateTime.UtcNow;
        }

        protected Product() { }

        public int Code { get; private set; }
        public string Name { get; private set; }
        public decimal Price { get; private set; }
        public Category Category { get; private set; }
        public string Description { get; private set; }
        public HashSet<string> IngredientsAdditionals { get; private set; }
        public Sizes Size { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public Guid CategoryId { get; private set; }

        public void ChangeCategory(Category category)
        {
            Category = category;
        }

        public void ChangeSizeProduct(Sizes size)
        {
            Size = size;
        }

        public void AddIngredients(string ingredient)
        {
            IngredientsAdditionals.Add(ingredient);
        }
    }
}
