﻿using MongoDB.Driver;
using RiseFood.WebApi.Config;
using RiseFood.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Services
{
    public class ProductService : IProductService
    {
        private readonly IMongoCollection<Product> _products;
        private readonly IMongoCollection<Category> _categories;

        public ProductService(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _products = database.GetCollection<Product>("products");
            _categories = database.GetCollection<Category>("categories");
        }


        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await _products.FindSync(product => true).ToListAsync();
        }

        public Task<Product> GetProduct(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<Product> GetProduct(int code)
        {
            throw new NotImplementedException();
        }

        public Task<Product> GetProduct(string name)
        {
            throw new NotImplementedException();
        }

        public void SaveProduct(Product product)
        {
            _products.InsertOne(product);
        }

        public void UpdateProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public void DeleteProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Dispose();
        }

        public void SaveCategory(Category category)
        {
            _categories.InsertOne(category);
        }

        public void UpdateCategory(Guid id)
        {
            throw new NotImplementedException();
        }

        public void DeteleCategory(Guid id)
        {
            throw new NotImplementedException();
        }

        public async  Task<IEnumerable<Category>> GetCategories()
        {
            return  _categories.Find(category => true).ToList();
        }

        public Task<IEnumerable<Product>> GetProductsByCategory(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
        