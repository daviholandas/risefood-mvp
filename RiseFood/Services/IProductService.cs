﻿using RiseFood.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Services
{
    public interface IProductService : IDisposable
    {
        Task<IEnumerable<Product>> GetAllProducts();
        Task<Product> GetProduct(Guid id);
        Task<Product> GetProduct(int code);
        Task<Product> GetProduct(string name);
        void SaveProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);

        void SaveCategory(Category category);
        void UpdateCategory(Guid id);
        void DeteleCategory(Guid id);
        Task<IEnumerable<Category>> GetCategories();
        Task<IEnumerable<Product>> GetProductsByCategory(Guid id);
    }
}
