﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RiseFood.WebApi.Config.AutoMapper;
using RiseFood.WebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Config
{
    public static class DependencyInjectioConfig
    {

        public static IServiceCollection InjectionDepencies(this IServiceCollection services, IConfiguration configuration)
        {
            //AutoMapper
            services.AddAutoMapper(typeof(ModelToDtoAutoMapperProfile), typeof(DtoToModelAutoMapperProfile));

            //MongoDb
            services.Configure<MongoDatabaseSettings>(configuration.GetSection(nameof(MongoDatabaseSettings)));
            services.AddSingleton<IMongoDatabaseSettings>(ct => ct.GetRequiredService<IOptions<MongoDatabaseSettings>>().Value);
           
            //Services
            services.AddSingleton<IProductService, ProductService>();

            return services;
        }
    }
}
