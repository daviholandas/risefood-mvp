﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using RiseFood.WebApi.Enums;
using RiseFood.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Config.Mapper
{
    public class ProductMapping
    {
        public static void Mapper()
        {
            BsonClassMap.RegisterClassMap<Product>( pm => 
            {
                pm.AutoMap();
                pm.MapMember(p => p.Code).SetIsRequired(true);
                pm.MapMember(p => p.Name).SetIsRequired(true);
                pm.MapMember(p => p.Price).SetSerializer(new DecimalSerializer(BsonType.Decimal128));
                pm.MapMember(p => p.Size).SetSerializer(new EnumSerializer<Sizes>(BsonType.String));
                pm.MapMember(p => p.CreatedDate).SetSerializer(new DateTimeSerializer(BsonType.DateTime));
                pm.UnmapMember(p => p.Category);
            });
        }
    }
}
