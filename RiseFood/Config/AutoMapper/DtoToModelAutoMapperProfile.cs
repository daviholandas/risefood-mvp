﻿using AutoMapper;
using RiseFood.WebApi.DTOs;
using RiseFood.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Config.AutoMapper
{
    public class DtoToModelAutoMapperProfile : Profile
    {
        public DtoToModelAutoMapperProfile()
        {
            CreateMap<ProductDto, Product>()
                .ConstructUsing(p => new Product(p.Code, p.Name, p.Price, p.Description, p.IngredientsAdditionals, p.Size, p.CreatedDate, p.CategoryId))
                .ForMember(p =>p.Category, opt => opt.Ignore());
            CreateMap<CategoryDto, Category>()
                .ConstructUsing(c => new Category(c.Code, c.Name));
        }
    }
}
