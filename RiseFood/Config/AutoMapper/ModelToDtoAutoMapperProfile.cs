﻿using AutoMapper;
using RiseFood.WebApi.DTOs;
using RiseFood.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Config.AutoMapper
{
    public class ModelToDtoAutoMapperProfile : Profile
    {
        public ModelToDtoAutoMapperProfile()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<Category, CategoryDto>();
        }
    }
}
