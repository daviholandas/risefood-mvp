﻿using Microsoft.Extensions.DependencyInjection;
using RiseFood.WebApi.Config.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Config
{
    public static class MongoDbMappers
    {
        public static void Mappers(this IServiceCollection services)
        {
            ProductMapping.Mapper();
        }
    }
}
