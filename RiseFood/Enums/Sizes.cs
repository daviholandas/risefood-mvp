﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.Enums
{
    public enum Sizes
    {
        Small,
        Medium,
        Large,
        Standard
    }
}
