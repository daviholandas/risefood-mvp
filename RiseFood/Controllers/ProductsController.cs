﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RiseFood.WebApi.DTOs;
using RiseFood.WebApi.Models;
using RiseFood.WebApi.Services;

namespace RiseFood.WebApi.Controllers
{
    [Route("api/[controller]")] 
    public class ProductsController : MainController
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductsController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<IEnumerable<ProductDto>> Get()
        {
            var products = _mapper.Map<IEnumerable<ProductDto>>(await _productService.GetAllProducts());
            return products;
        }

        // GET: api/Products/categories
        [HttpGet("categories")]
        public async Task<IEnumerable<CategoryDto>> GetCategories(int id)
        {
            return _mapper.Map<IEnumerable<CategoryDto>>(await _productService.GetCategories());
        }

        // POST: api/Products
        [HttpPost]
        public void Post([FromBody] ProductDto productDto)
        {
            var product = _mapper.Map<Product>(productDto);
            _productService.SaveProduct(product);
        }

        // PUT: api/Products/5
        [HttpPost("categories")]
        public async Task<ActionResult> PostCategories([FromBody] CategoryDto categoryDto)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState.ErrorCount);
            }

            
            var category = _mapper.Map<Category>(categoryDto);
             _productService.SaveCategory(category);

            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
