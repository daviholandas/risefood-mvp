﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using RiseFood.WebApi.Enums;
using RiseFood.WebApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.DTOs
{
    public class ProductDto
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage ="O {0} é obrigatorio.")]
        [Range(1, 999, ErrorMessage ="O {0} está fora do tamanho permitido.")]
        public int Code { get; set; }
        [Required(ErrorMessage = "O {0} é obrigatorio.")]
        public string Name { get;  set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Price { get;  set; }
        public Category Category { get;  set; }
        public string Description { get;  set; }
        public HashSet<string> IngredientsAdditionals { get;  set; }
        [BsonRepresentation(BsonType.String)]
        public Sizes Size { get; set; }

        [DisplayFormat(DataFormatString = "dd/mm/yyyy")]
        public DateTime CreatedDate { get;  set; }
        public Guid CategoryId { get; set; }

    }
}
