﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiseFood.WebApi.DTOs
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        [Required]
        [Range(1, 999, ErrorMessage = "O {0} está fora do tamanho permitido.")]
        public int Code { get; set; }
        [Required]
        [MinLength(3, ErrorMessage ="O campo {0} não pode ter menos de que 3 letras.")]
        public string Name { get; set; }
    }
}
